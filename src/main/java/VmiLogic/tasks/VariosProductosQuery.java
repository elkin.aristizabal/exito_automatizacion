package VmiLogic.tasks;
import VmiLogic.utils.Conexion;
import VmiLogic.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VariosProductosQuery implements Task {

    String ID = "1211713388444-01";
    public static String recipienNAme;
    public static String Adress;

    @Override
    public <T extends Actor> void performAs(T actor) {
        Conexion conexionOrder = null;
        Conexion conexionPackage = null;
        conexionOrder = new Conexion(Constants.TRANPORTORDER);
        conexionPackage = new Conexion(Constants.TRANSPORTPACKAGE);

        recipienNAme = (String) conexionOrder.mostrar("recipientName", ID, true);
        Adress = (String) conexionOrder.mostrar("recipientAddress", ID, true);
        conexionOrder.delete(ID,true);
        conexionPackage.delete(ID,false);

    }


    public static VariosProductosQuery Query() {
        return instrumented(VariosProductosQuery.class);
    }
}

