package VmiLogic.tasks;

import VmiLogic.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static VmiLogic.utils.Producer.sendMessage;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VariosProductosRabbitMq implements Task

{
    @Override
    public <T extends Actor> void performAs(T actor) {
            sendMessage(Constants.QUEUEORDERDATA);
            sendMessage(Constants.QUEUEPACKAGEDATA);
    }
        public static VariosProductosRabbitMq Message()
        {
            return instrumented(VariosProductosRabbitMq.class);
        }
}

