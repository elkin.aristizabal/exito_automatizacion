package VmiLogic.utils;


import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ReadJsonFile {
    public static String ReadJson (String archivo) throws FileNotFoundException, ParseException {
        Object ob = new JSONParser().parse(new FileReader(archivo));
        String message =String.valueOf(ob);
        return message;
    }
}
