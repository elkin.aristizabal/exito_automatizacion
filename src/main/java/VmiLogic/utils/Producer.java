package VmiLogic.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import net.minidev.json.parser.ParseException;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class Producer

{
    public static void sendMessage(String queue) {

        try {
            String MESSAGE = ReadJsonFile.ReadJson("src/main/java/VmiLogic/messages/PedidoVMI.json");
            String QUEQUE = queue;
            String EXCHANGE = "domainEvents";
            String ROUTINGKEY="tracking.guide";
            Connection connection = ConnectionUtils.getConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(QUEQUE,true,false,false, null);
            channel.basicPublish("",QUEQUE,null,MESSAGE.getBytes());
            channel.close();
            connection.close();
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
