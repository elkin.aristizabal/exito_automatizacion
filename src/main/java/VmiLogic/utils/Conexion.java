package VmiLogic.utils;
import com.mongodb.*;

public class Conexion {



    DB BaseDatos;
    DBCollection coleccion;
    BasicDBObject documento = new BasicDBObject();

    public Conexion(String colleccion) {

        MongoClientURI uri = new MongoClientURI(Constants.URIMONGO);
        MongoClient mongoClient = new MongoClient(uri);
        BaseDatos = mongoClient.getDB(Constants.DATABASE);
        coleccion = BaseDatos.getCollection(colleccion);

    }

    public Object mostrar (String llave, String id, boolean orden){
        DBObject query = null;
        if (orden==true) {
            query = new BasicDBObject("_id", id);
        }
        else{
            query = new BasicDBObject("_id.orderNumber", id);
        }
        DBCursor cursor = coleccion.find(query);
        DBObject pedido = cursor.one();
        System.out.println(pedido);
        return pedido.get(llave);
    }

    public void delete (String id , boolean orden)
    {
        DBObject query = null;
        if (orden==true) {
            query = new BasicDBObject("_id", id);
        }
        else{
            query = new BasicDBObject("_id.orderNumber", id);
        }
        coleccion.remove(query);
    }

}
