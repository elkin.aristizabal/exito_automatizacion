package VmiLogic.utils;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class ConnectionUtils

{
    public static Connection getConnection() throws IOException, TimeoutException, NoSuchAlgorithmException, KeyManagementException

    {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("elegant-badger.rmq.cloudamqp.com");
        factory.setVirtualHost("qa");
        factory.setUsername("qa-gdd-quind");
        factory.setPassword("JD3mhglSblCq");
        factory.setPort(5671);
        factory.useSslProtocol("SSL");
        return factory.newConnection();

    }
}