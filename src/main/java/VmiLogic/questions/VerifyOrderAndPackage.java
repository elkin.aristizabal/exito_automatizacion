package VmiLogic.questions;

import VmiLogic.tasks.VariosProductosQuery;
import VmiLogic.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class VerifyOrderAndPackage implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean compare;
        String recipientNAme = VariosProductosQuery.recipienNAme;
        if  (recipientNAme.equals(Constants.RECIPIENTNAME)){
            compare=true;
        }
        else {
            compare = false;
        }
        return compare;
    }
    public static VerifyOrderAndPackage verifyOrderAndPackage() {return  new VerifyOrderAndPackage();}
}
