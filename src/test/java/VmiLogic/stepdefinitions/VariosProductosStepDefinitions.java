package VmiLogic.stepdefinitions;

import VmiLogic.questions.VerifyOrderAndPackage;
import VmiLogic.tasks.VariosProductosQuery;
import VmiLogic.tasks.VariosProductosRabbitMq;
import VmiLogic.utils.Conexion;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.ensure.Ensure;
import org.hamcrest.Matchers;

public class VariosProductosStepDefinitions {

    private Actor Andrea = Actor.named("Andrea");


    @Given("^tengo un mensaje en la cola OMSTransportIntegratorqueue con tipo de negocio \\(VMI\\) y con varios productos de varios proveedores$")
    public void tengoUnMensajeEnLaColaOMSTransportIntegratorqueueConTipoDeNegocioVMIYConVariosProductosDeVariosProveedores() {
        // Write code here that turns the phrase above into concrete actions

         Andrea.attemptsTo((VariosProductosRabbitMq.Message()));
    }


    @When("^el QA haga la consulta en la coleccion transportOrderData$")
    public void elQAHagaLaConsultaEnLaColeccionTransportOrderData() {
        // Write code here that turns the phrase above into concrete actions

        Andrea.attemptsTo(VariosProductosQuery.Query());

    }

    @Then("^el QA deberia ver un registro correspondiente al mensaje inyectado que contenga los productos de éste mismo$")
    public void elQADeberiaVerUnRegistroCorrespondienteAlMensajeInyectadoQueContengaLosProductosDeÉsteMismo() {

        Andrea.should(GivenWhenThen.seeThat(VerifyOrderAndPackage.verifyOrderAndPackage(), Matchers.is(true)));

    }


}
